from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.search),
    url(r'^tutorial/$', views.tutorial),
    url('^addtutorial/$', views.addtutorial),
]