from django.db import models
from datetime import datetime


class Tutorial(models.Model):
    tag = models.CharField(max_length=20)
    description = models.CharField(max_length=50)
    tutorial_data = models.CharField(max_length=20000)


class Comment(models.Model):
    tutorial_id = models.IntegerField()
    user_id = models.IntegerField()
    comment = models.CharField(max_length=500)
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(blank=True, null=True)

