# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('userdetails', '0003_auto_20151128_1529'),
    ]

    operations = [
        migrations.CreateModel(
            name='Friend',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.IntegerField()),
                ('friend_id', models.IntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='userdata',
            name='last_login',
        ),
        migrations.RemoveField(
            model_name='userdata',
            name='password',
        ),
        migrations.AddField(
            model_name='userdata',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 28, 18, 48, 19, 374936), verbose_name='Created at', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userdata',
            name='is_deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userdata',
            name='modified_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 28, 18, 48, 33, 291312), verbose_name='Modified at', auto_now=True),
            preserve_default=False,
        ),
    ]
