# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('userdetails', '0002_userdata_gender'),
    ]

    operations = [
        migrations.AddField(
            model_name='userdata',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
        migrations.AddField(
            model_name='userdata',
            name='password',
            field=models.CharField(default=datetime.datetime(2015, 11, 28, 15, 29, 11, 423186, tzinfo=utc), max_length=128, verbose_name='password'),
            preserve_default=False,
        ),
    ]
