# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userdetails', '0007_skill'),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified at')),
                ('user_id', models.IntegerField()),
                ('blog_data', models.CharField(default=None, max_length=5000)),
                ('blog_name', models.CharField(max_length=100)),
                ('blog_tags', models.CharField(max_length=20)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
