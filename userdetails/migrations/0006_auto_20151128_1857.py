# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('userdetails', '0005_auto_20151128_1855'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdata',
            name='last_login',
        ),
        migrations.RemoveField(
            model_name='userdata',
            name='password',
        ),
        migrations.AddField(
            model_name='userdata',
            name='is_deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userdata',
            name='modified_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 28, 18, 57, 8, 359724), verbose_name='Modified at', auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='userdata',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created at'),
        ),
    ]
