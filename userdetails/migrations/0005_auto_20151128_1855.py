# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('userdetails', '0004_auto_20151128_1848'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdata',
            name='is_deleted',
        ),
        migrations.RemoveField(
            model_name='userdata',
            name='modified_at',
        ),
        migrations.AddField(
            model_name='userdata',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
        migrations.AddField(
            model_name='userdata',
            name='password',
            field=models.CharField(default=123456, max_length=128, verbose_name='password'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='userdata',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 28, 18, 55, 32, 331581)),
        ),
    ]
