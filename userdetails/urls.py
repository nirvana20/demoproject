from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.login, name='index'),
    url(r'^signup/$', views.signup),
    url(r'^blog/$', views.blog),
]