from django.db import models
from django.contrib.auth.models import AbstractBaseUser, User
# Create your models here.
from djutil.models import TimeStampedModel


class ModelBase(TimeStampedModel):
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        """ On save, update timestamps """
        # self.modified_at = datetime.now()
        return super(ModelBase, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """ Soft delete """
        self.is_deleted = True
        self.save()


class Userdata(ModelBase,AbstractBaseUser):

    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    phone_number = models.IntegerField()
    email = models.EmailField(max_length=50)


    gender_choices = (
        ('M', 'Male'),
        ('F', 'Female')
    )
    gender = models.CharField(max_length=1, choices=gender_choices,default='M')
    USERNAME_FIELD = 'email'


class Friend(models.Model):

    user_id = models.IntegerField()
    friend_id = models.IntegerField()


class Skill(models.Model):
    user_id = models.IntegerField()
    skills_name = models.CharField(max_length=20,default=None)


class Blog(TimeStampedModel):
    user_id = models.IntegerField()
    blog_data = models.CharField(max_length=5000,default=None)
    blog_name = models.CharField(max_length=100)
    blog_tags = models.CharField(max_length=20)



